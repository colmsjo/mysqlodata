OData producer on top of MySQL
==============================

OData producer built using these libraries:

 * http://odatamysqlphpconnect.codeplex.com/
 * http://odataphpproducer.codeplex.com


Usage
-----

Prerequisites:

 * docker needs to be installed.

Installation: `docker build --rm -t mysqlodata .`

Run the container in daemon mode: `docker run -d -p 80:80 -p 443:443 [IMAGE ID]`. The 80 and 443 ports that have been exposed from the container will be routed from the host to the container using the `-p` flag.


Interactive mode
----------------

When developing, it is usefull to connect to the containers shell:

```
# Start a container and connect to the shell (remove when stopped)
docker run -t -i --rm -p 80:80 -p 443:443 -v /mnt2:/volume mysqlodata /bin/bash

# Start the services
supervisord &> /tmp/out.txt &

# Check that all processes are up
ps -ef
```

Setup OData services
--------------------

The OData services to be exposed needs to be defined. The MySQConnector generates this definitions from the
database structure.

Start a docker container: `docker run -t -i --rm mysqlodata /bin/bash`

You need to use PHP version 5.3.x for the connector (5.3.28 is installed using phpbrew in the `Dockerfile`). Initialize PHP like this: `. ~/.phpbrew/bashrc; phpbrew use 5.3.28`

Generate EDMX-files: `php MySQLConnector.php /db=[DB Name] /srvc=[Service Name] /u=[MySQL Username] /pw=[MySQL Password] [/p=[port of MySQL Server]] /h=[host name of MySQL server]`

Generate all services: `php MySQLConnector.php /srvc=[Service Name]`

For the MySQL server in the container: `cd /MySQLConnector; php MySQLConnector.php /db=gizur_com /srvc=gizur_com /u=admin /pw=mysql-server  /h=localhost`

Followed by: `php MySQLConnector.php  /srvc=gizur_com`

Make the result available in the host: `cp -r /MySQLConnector/ODataConnectorForMySQL/OutputFiles/gizur_com /volume/gizur_com`. The generated files are now available at /mnt2.

Publish the service on the OData server: `cp -r /MySQLConnector/ODataConnectorForMySQL/OutputFiles/gizur_com /OData_Producer_for_PHP/services`

Test the service: `curl http://localhost/gizur_com.svc`

Start a docker container in daemon mode: `docker run -d -p 80:80 -p 443:443 -v /mnt2:/volume [IMAGE ID] /bin/bash`

Test that things work
---------------------

Here are some queries, just to get you started:

```
# List all entity types (tables)
curl http://localhost/gizur_com.svc/$metadata

# Get all rows in wp_options in json format
curl http://localhost:8080/gizur_com.svc/wp_options?$format=json

# Get the first entry in the table wp_options
curl http://localhost/gizur_com.svc/wp_options\(1\)\?\$format=json
```



Production
----------

The included MySQL server should not be used for production. Disable it by commenting out the
`[program:mysql]` parts with `#` in supervisord.conf

MySQL credentials for external server should be passed as environment variables that are set when starting the container.

Here is an example: `docker run -d -p 80:80 -p 443:443 -e USERNAME="admin", PASSWORD="secret", HOSTNAME="hostname" base /bin/bash`



Notes
-----

1. The frameworks only works with PHP 5.3.x. phpbrew is used to handle PHP versions.

2. There are a few backslashed in some paths in the library. This don't work in Unix.

These are the files that needs to be fixed. Patched versions are store in src-fixed-odata

```
ODataProducer/library/ODataProducer/Writers/Metadata/MetadataAssociationTypeSet.php
require_once 'ODataProducer\Writers\Metadata\MetadataBase.php';
require_once 'ODataProducer/Writers/Metadata/MetadataBase.php';

ODataProducer/library/ODataProducer/Providers/Metadata/ServiceBaseMetadata.php
require_once 'ODataProducer\Providers\Metadata\ResourceTypeKind.php';
require_once 'ODataProducer/Providers/Metadata/ResourceTypeKind.php';

require_once 'ODataProducer\Providers\Metadata\ResourceType.php';
require_once 'ODataProducer/Providers/Metadata/ResourceType.php';

require_once 'ODataProducer\Providers\Metadata\IDataServiceMetadataProvider.php';
require_once 'ODataProducer/Providers/Metadata/IDataServiceMetadataProvider.php';


ODataProducer/library/ODataProducer/Providers/Stream/IDataServiceStreamProvider2.php
require_once 'ODataProducer\Providers\Stream\IDataServiceStreamProvider.php';
require_once 'ODataProducer/Providers/Stream/IDataServiceStreamProvider.php';
```



