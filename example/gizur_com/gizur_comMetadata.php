<?php

/** 
 * Implementation of IDataServiceMetadataProvider.
 * 
 * PHP version 5.3
 * 
 * @category  Service
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   SVN: 1.0
 * @link      http://odataphpproducer.codeplex.com
 * 
 */
use ODataProducer\Providers\Metadata\ResourceStreamInfo;
use ODataProducer\Providers\Metadata\ResourceAssociationSetEnd;
use ODataProducer\Providers\Metadata\ResourceAssociationSet;
use ODataProducer\Common\NotImplementedException;
use ODataProducer\Providers\Metadata\Type\EdmPrimitiveType;
use ODataProducer\Providers\Metadata\ResourceSet;
use ODataProducer\Providers\Metadata\ResourcePropertyKind;
use ODataProducer\Providers\Metadata\ResourceProperty;
use ODataProducer\Providers\Metadata\ResourceTypeKind;
use ODataProducer\Providers\Metadata\ResourceType;
use ODataProducer\Common\InvalidOperationException;
use ODataProducer\Providers\Metadata\IDataServiceMetadataProvider;
require_once 'ODataProducer/Providers/Metadata/IDataServiceMetadataProvider.php';
use ODataProducer\Providers\Metadata\ServiceBaseMetadata;
//Begin Resource Classes

/**
 * wp_commentmetum entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_commentmetum
{
    //Edm.Int64
    public $meta_id;
            
    //Edm.Int64
    public $comment_id;
            
    //Edm.String
    public $meta_key;
            
    //Edm.String
    public $meta_value;
            
}

/**
 * wp_comment entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_comment
{
    //Edm.Int64
    public $comment_ID;
            
    //Edm.Int64
    public $comment_post_ID;
            
    //Edm.String
    public $comment_author;
            
    //Edm.String
    public $comment_author_email;
            
    //Edm.String
    public $comment_author_url;
            
    //Edm.String
    public $comment_author_IP;
            
    //Edm.DateTime
    public $comment_date;
            
    //Edm.DateTime
    public $comment_date_gmt;
            
    //Edm.String
    public $comment_content;
            
    //Edm.Int32
    public $comment_karma;
            
    //Edm.String
    public $comment_approved;
            
    //Edm.String
    public $comment_agent;
            
    //Edm.String
    public $comment_type;
            
    //Edm.Int64
    public $comment_parent;
            
    //Edm.Int64
    public $user_id;
            
}

/**
 * wp_link entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_link
{
    //Edm.Int64
    public $link_id;
            
    //Edm.String
    public $link_url;
            
    //Edm.String
    public $link_name;
            
    //Edm.String
    public $link_image;
            
    //Edm.String
    public $link_target;
            
    //Edm.String
    public $link_description;
            
    //Edm.String
    public $link_visible;
            
    //Edm.Int64
    public $link_owner;
            
    //Edm.Int32
    public $link_rating;
            
    //Edm.DateTime
    public $link_updated;
            
    //Edm.String
    public $link_rel;
            
    //Edm.String
    public $link_notes;
            
    //Edm.String
    public $link_rss;
            
}

/**
 * wp_option entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_option
{
    //Edm.Int64
    public $option_id;
            
    //Edm.String
    public $option_name;
            
    //Edm.String
    public $option_value;
            
    //Edm.String
    public $autoload;
            
}

/**
 * wp_postmetum entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_postmetum
{
    //Edm.Int64
    public $meta_id;
            
    //Edm.Int64
    public $post_id;
            
    //Edm.String
    public $meta_key;
            
    //Edm.String
    public $meta_value;
            
}

/**
 * wp_post entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_post
{
    //Edm.Int64
    public $ID;
            
    //Edm.Int64
    public $post_author;
            
    //Edm.DateTime
    public $post_date;
            
    //Edm.DateTime
    public $post_date_gmt;
            
    //Edm.String
    public $post_content;
            
    //Edm.String
    public $post_title;
            
    //Edm.String
    public $post_excerpt;
            
    //Edm.String
    public $post_status;
            
    //Edm.String
    public $comment_status;
            
    //Edm.String
    public $ping_status;
            
    //Edm.String
    public $post_password;
            
    //Edm.String
    public $post_name;
            
    //Edm.String
    public $to_ping;
            
    //Edm.String
    public $pinged;
            
    //Edm.DateTime
    public $post_modified;
            
    //Edm.DateTime
    public $post_modified_gmt;
            
    //Edm.String
    public $post_content_filtered;
            
    //Edm.Int64
    public $post_parent;
            
    //Edm.String
    public $guid;
            
    //Edm.Int32
    public $menu_order;
            
    //Edm.String
    public $post_type;
            
    //Edm.String
    public $post_mime_type;
            
    //Edm.Int64
    public $comment_count;
            
}

/**
 * wp_term_relationship entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_term_relationship
{
    //Edm.Int64
    public $object_id;
            
    //Edm.Int64
    public $term_taxonomy_id;
            
    //Edm.Int32
    public $term_order;
            
}

/**
 * wp_term_taxonomy entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_term_taxonomy
{
    //Edm.Int64
    public $term_taxonomy_id;
            
    //Edm.Int64
    public $term_id;
            
    //Edm.String
    public $taxonomy;
            
    //Edm.String
    public $description;
            
    //Edm.Int64
    public $parent;
            
    //Edm.Int64
    public $count;
            
}

/**
 * wp_term entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_term
{
    //Edm.Int64
    public $term_id;
            
    //Edm.String
    public $name;
            
    //Edm.String
    public $slug;
            
    //Edm.Int64
    public $term_group;
            
}

/**
 * wp_usermetum entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_usermetum
{
    //Edm.Int64
    public $umeta_id;
            
    //Edm.Int64
    public $user_id;
            
    //Edm.String
    public $meta_key;
            
    //Edm.String
    public $meta_value;
            
}

/**
 * wp_user entity type.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class wp_user
{
    //Edm.Int64
    public $ID;
            
    //Edm.String
    public $user_login;
            
    //Edm.String
    public $user_pass;
            
    //Edm.String
    public $user_nicename;
            
    //Edm.String
    public $user_email;
            
    //Edm.String
    public $user_url;
            
    //Edm.DateTime
    public $user_registered;
            
    //Edm.String
    public $user_activation_key;
            
    //Edm.Int32
    public $user_status;
            
    //Edm.String
    public $display_name;
            
}


/**
 * Create gizur_com metadata.
 * 
 * @category  Service
 * @package   Service_gizur_com
 * @author    Yash K. Kothari <odataphpproducer_alias@microsoft.com>
 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
 * @version   Release: 1.0
 * @link      http://odataphpproducer.codeplex.com
 */
class Creategizur_comMetadata
{
    /**
     * create metadata
     * 
     * @return gizur_comMetadata
     */
    public static function create()
    {
        $metadata = new ServiceBaseMetadata('gizur_comEntities', 'gizur_com');
        
        //Register the entity (resource) type 'wp_commentmetum'
        $wp_commentmetumEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_commentmetum'), 'wp_commentmetum', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_commentmetumEntityType, 'meta_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_commentmetumEntityType, 'comment_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_commentmetumEntityType, 'meta_key', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentmetumEntityType, 'meta_value', EdmPrimitiveType::STRING
        );
        
        //Register the entity (resource) type 'wp_comment'
        $wp_commentEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_comment'), 'wp_comment', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_commentEntityType, 'comment_ID', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_post_ID', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_author', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_author_email', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_author_url', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_author_IP', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_date', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_date_gmt', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_content', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_karma', EdmPrimitiveType::INT32
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_approved', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_agent', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_type', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'comment_parent', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_commentEntityType, 'user_id', EdmPrimitiveType::INT64
        );
        
        //Register the entity (resource) type 'wp_link'
        $wp_linkEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_link'), 'wp_link', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_linkEntityType, 'link_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_url', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_name', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_image', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_target', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_description', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_visible', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_owner', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_rating', EdmPrimitiveType::INT32
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_updated', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_rel', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_notes', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_linkEntityType, 'link_rss', EdmPrimitiveType::STRING
        );
        
        //Register the entity (resource) type 'wp_option'
        $wp_optionEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_option'), 'wp_option', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_optionEntityType, 'option_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_optionEntityType, 'option_name', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_optionEntityType, 'option_value', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_optionEntityType, 'autoload', EdmPrimitiveType::STRING
        );
        
        //Register the entity (resource) type 'wp_postmetum'
        $wp_postmetumEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_postmetum'), 'wp_postmetum', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_postmetumEntityType, 'meta_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_postmetumEntityType, 'post_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_postmetumEntityType, 'meta_key', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postmetumEntityType, 'meta_value', EdmPrimitiveType::STRING
        );
        
        //Register the entity (resource) type 'wp_post'
        $wp_postEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_post'), 'wp_post', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_postEntityType, 'ID', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_author', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_date', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_date_gmt', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_content', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_title', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_excerpt', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_status', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'comment_status', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'ping_status', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_password', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_name', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'to_ping', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'pinged', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_modified', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_modified_gmt', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_content_filtered', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_parent', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'guid', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'menu_order', EdmPrimitiveType::INT32
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_type', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'post_mime_type', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_postEntityType, 'comment_count', EdmPrimitiveType::INT64
        );
        
        //Register the entity (resource) type 'wp_term_relationship'
        $wp_term_relationshipEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_term_relationship'), 'wp_term_relationship', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_term_relationshipEntityType, 'object_id', EdmPrimitiveType::INT64
        );
        $metadata->addKeyProperty(
            $wp_term_relationshipEntityType, 'term_taxonomy_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_term_relationshipEntityType, 'term_order', EdmPrimitiveType::INT32
        );
        
        //Register the entity (resource) type 'wp_term_taxonomy'
        $wp_term_taxonomyEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_term_taxonomy'), 'wp_term_taxonomy', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_term_taxonomyEntityType, 'term_taxonomy_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_term_taxonomyEntityType, 'term_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_term_taxonomyEntityType, 'taxonomy', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_term_taxonomyEntityType, 'description', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_term_taxonomyEntityType, 'parent', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_term_taxonomyEntityType, 'count', EdmPrimitiveType::INT64
        );
        
        //Register the entity (resource) type 'wp_term'
        $wp_termEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_term'), 'wp_term', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_termEntityType, 'term_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_termEntityType, 'name', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_termEntityType, 'slug', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_termEntityType, 'term_group', EdmPrimitiveType::INT64
        );
        
        //Register the entity (resource) type 'wp_usermetum'
        $wp_usermetumEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_usermetum'), 'wp_usermetum', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_usermetumEntityType, 'umeta_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_usermetumEntityType, 'user_id', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_usermetumEntityType, 'meta_key', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_usermetumEntityType, 'meta_value', EdmPrimitiveType::STRING
        );
        
        //Register the entity (resource) type 'wp_user'
        $wp_userEntityType = $metadata->addEntityType(
            new ReflectionClass('wp_user'), 'wp_user', 'gizur_com'
        );
        $metadata->addKeyProperty(
            $wp_userEntityType, 'ID', EdmPrimitiveType::INT64
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_login', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_pass', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_nicename', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_email', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_url', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_registered', EdmPrimitiveType::DATETIME
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_activation_key', EdmPrimitiveType::STRING
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'user_status', EdmPrimitiveType::INT32
        );
        $metadata->addPrimitiveProperty(
            $wp_userEntityType, 'display_name', EdmPrimitiveType::STRING
        );
        
        $wp_commentmetasResourceSet = $metadata->addResourceSet(
            'wp_commentmetas', $wp_commentmetumEntityType
        );
        $wp_commentsResourceSet = $metadata->addResourceSet(
            'wp_comments', $wp_commentEntityType
        );
        $wp_linksResourceSet = $metadata->addResourceSet(
            'wp_links', $wp_linkEntityType
        );
        $wp_optionsResourceSet = $metadata->addResourceSet(
            'wp_options', $wp_optionEntityType
        );
        $wp_postmetasResourceSet = $metadata->addResourceSet(
            'wp_postmetas', $wp_postmetumEntityType
        );
        $wp_postsResourceSet = $metadata->addResourceSet(
            'wp_posts', $wp_postEntityType
        );
        $wp_term_relationshipsResourceSet = $metadata->addResourceSet(
            'wp_term_relationships', $wp_term_relationshipEntityType
        );
        $wp_term_taxonomiesResourceSet = $metadata->addResourceSet(
            'wp_term_taxonomies', $wp_term_taxonomyEntityType
        );
        $wp_termsResourceSet = $metadata->addResourceSet(
            'wp_terms', $wp_termEntityType
        );
        $wp_usermetasResourceSet = $metadata->addResourceSet(
            'wp_usermetas', $wp_usermetumEntityType
        );
        $wp_usersResourceSet = $metadata->addResourceSet(
            'wp_users', $wp_userEntityType
        );

        //Register the assoications (navigations)
        
        return $metadata;
    }
}
?>
