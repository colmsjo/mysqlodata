<?php  	
 

	/** 
	 * Implementation of IDataServiceQueryProvider.
	 * 
	 * PHP version 5.3
	 * 
	 * @category  Service
	 * @package   gizur_com;
	 * @author    MySQLConnector <odataphpproducer_alias@microsoft.com>
	 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
	 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
	 * @version   SVN: 1.0
	 * @link      http://odataphpproducer.codeplex.com
	 */     
	use ODataProducer\UriProcessor\ResourcePathProcessor\SegmentParser\KeyDescriptor;
	use ODataProducer\Providers\Metadata\ResourceSet;
	use ODataProducer\Providers\Metadata\ResourceProperty;
	use ODataProducer\Providers\Query\IDataServiceQueryProvider2;
	require_once "gizur_comMetadata.php";
	require_once "ODataProducer/Providers/Query/IDataServiceQueryProvider2.php";
	
	/** The name of the database for gizur_com*/
	define('DB_NAME', "gizur_com");
	/** MySQL database username */
	define('DB_USER', "admin");
	/** MySQL database password */
	define('DB_PASSWORD', "mysql-server");
	/** MySQL hostname */
	define('DB_HOST', "localhost");
			
   			
	/**
     * gizur_comQueryProvider implemetation of IDataServiceQueryProvider2.
	 * @category  Service
	 * @package   gizur_com;
	 * @author    MySQLConnector <odataphpproducer_alias@microsoft.com>
	 * @copyright 2011 Microsoft Corp. (http://www.microsoft.com)
	 * @license   New BSD license, (http://www.opensource.org/licenses/bsd-license.php)
	 * @version   Release: 1.0
	 * @link      http://odataphpproducer.codeplex.com
	 */
	class gizur_comQueryProvider implements IDataServiceQueryProvider2
	{
    	/**
     	 * Handle to connection to Database     
     	 */
    	private $_connectionHandle = null;

      private $_expressionProvider = null;

    	/**
     	 * Constructs a new instance of gizur_comQueryProvider
     	 * 
     	 */
	    public function __construct()
    	{
        	$this->_connectionHandle = @mysql_connect(DB_HOST, DB_USER, DB_PASSWORD, true);
        	if ( $this->_connectionHandle ) {
        		mysql_select_db(DB_NAME, $this->_connectionHandle);
        	} else {             
            	die(mysql_error());
        	} 
    	}

	    /**
    	 * Gets collection of entities belongs to an entity set
     	 * 
     	 * @param ResourceSet $resourceSet The entity set whose entities needs to be fetched
     	 * 
     	 * @return array(Object)
     	 */
    	public function getResourceSet(ResourceSet $resourceSet, $filterOption = null, 
        	$select=null, $orderby=null, $top=null, $skip=null)
    	{   
        	$resourceSetName =  $resourceSet->getName();
			if( $resourceSetName !== 'wp_commentmeta'
	        			 
        	and $resourceSetName !== 'wp_comments'
        				 
        	and $resourceSetName !== 'wp_links'
        				 
        	and $resourceSetName !== 'wp_options'
        				
	    	and $resourceSetName !== 'wp_postmeta'
	        			 
        	and $resourceSetName !== 'wp_posts'
        				 
        	and $resourceSetName !== 'wp_term_relationships'
        				
	    	and $resourceSetName !== 'wp_term_taxonomy'
	        			 
        	and $resourceSetName !== 'wp_terms'
        				
	    	and $resourceSetName !== 'wp_usermeta'
	        			 
        	and $resourceSetName !== 'wp_users'
        				)	       		
        	{
        		die('(gizur_comQueryProvider) Unknown resource set ' . $resourceSetName);
        	}
			 
        	if($resourceSetName === 'wp_commentmetas')
        	{
        		$resourceSetName = 'wp_commentmeta';
        	}	
       				 
        	if($resourceSetName === 'wp_postmetas')
        	{
        		$resourceSetName = 'wp_postmeta';
        	}	
       				 
        	if($resourceSetName === 'wp_term_taxonomies')
        	{
        		$resourceSetName = 'wp_term_taxonomy';
        	}	
       				 
        	if($resourceSetName === 'wp_usermetas')
        	{
        		$resourceSetName = 'wp_usermeta';
        	}	
       				        	
        	$query = "SELECT * FROM $resourceSetName";
	        if ($filterOption != null) {
    	        $query .= ' WHERE ' . $filterOption;
        	}
        	$stmt = mysql_query($query);
        	if ($stmt === false) {
            	die(print_r(mysql_error(), true));
        	}

        	$returnResult = array();
        	switch ($resourceSetName) {
        		
				case 'wp_commentmeta':
	        		
	        		$returnResult = $this->_serializewp_commentmetas($stmt);
       				break;
				
				case 'wp_comments':
	        		
	        		$returnResult = $this->_serializewp_comments($stmt);
       				break;
				
				case 'wp_links':
	        		
	        		$returnResult = $this->_serializewp_links($stmt);
       				break;
				
				case 'wp_options':
	        		
	        		$returnResult = $this->_serializewp_options($stmt);
       				break;
				
				case 'wp_postmeta':
	        		
	        		$returnResult = $this->_serializewp_postmetas($stmt);
       				break;
				
				case 'wp_posts':
	        		
	        		$returnResult = $this->_serializewp_posts($stmt);
       				break;
				
				case 'wp_term_relationships':
	        		
	        		$returnResult = $this->_serializewp_term_relationships($stmt);
       				break;
				
				case 'wp_term_taxonomy':
	        		
	        		$returnResult = $this->_serializewp_term_taxonomies($stmt);
       				break;
				
				case 'wp_terms':
	        		
	        		$returnResult = $this->_serializewp_terms($stmt);
       				break;
				
				case 'wp_usermeta':
	        		
	        		$returnResult = $this->_serializewp_usermetas($stmt);
       				break;
				
				case 'wp_users':
	        		
	        		$returnResult = $this->_serializewp_users($stmt);
       				break;
				
        	}
        	mysql_free_result($stmt);
        	return $returnResult;        
		} 


	    /**
    	 * Gets an entity instance from an entity set identifed by a key
	     * 
    	 * @param ResourceSet   $resourceSet   The entity set from which 
	     *                                     an entity needs to be fetched
    	 * @param KeyDescriptor $keyDescriptor The key to identify the entity to be fetched
     	 * 
	     * @return Object/NULL Returns entity instance if found else null
    	 */
	    public function getResourceFromResourceSet(ResourceSet $resourceSet, KeyDescriptor $keyDescriptor)
    	{   
        	$resourceSetName =  $resourceSet->getName();
        	if( $resourceSetName !== 'wp_commentmeta'
	        			 
        	and $resourceSetName !== 'wp_comments'
        				 
        	and $resourceSetName !== 'wp_links'
        				 
        	and $resourceSetName !== 'wp_options'
        				
	    	and $resourceSetName !== 'wp_postmeta'
	        			 
        	and $resourceSetName !== 'wp_posts'
        				 
        	and $resourceSetName !== 'wp_term_relationships'
        				
	    	and $resourceSetName !== 'wp_term_taxonomy'
	        			 
        	and $resourceSetName !== 'wp_terms'
        				
	    	and $resourceSetName !== 'wp_usermeta'
	        			 
        	and $resourceSetName !== 'wp_users'
        				)	       		
        	{
	        	die('(gizur_comQueryProvider) Unknown resource set ' . $resourceSetName);
    	    }
    	     
        	if($resourceSetName === 'wp_commentmetas')
        	{
        		$resourceSetName = 'wp_commentmeta';
        	}	
       				 
        	if($resourceSetName === 'wp_postmetas')
        	{
        		$resourceSetName = 'wp_postmeta';
        	}	
       				 
        	if($resourceSetName === 'wp_term_taxonomies')
        	{
        		$resourceSetName = 'wp_term_taxonomy';
        	}	
       				 
        	if($resourceSetName === 'wp_usermetas')
        	{
        		$resourceSetName = 'wp_usermeta';
        	}	
       				
    	
        	$namedKeyValues = $keyDescriptor->getValidatedNamedValues();
        	$condition = null;
        	foreach ($namedKeyValues as $key => $value) {
	            $condition .= $key . ' = ' . $value[0] . ' and ';
    	    }
	
    	    $len = strlen($condition);
        	$condition = substr($condition, 0, $len - 5); 
	        $query = "SELECT * FROM $resourceSetName WHERE $condition";
    	    $stmt = mysql_query($query);
        	if ($stmt === false) {
            	die(print_r(mysql_error(), true));
        	}

        	//If resource not found return null to the library
        	if (!mysql_num_rows($stmt)) {
            	return null;
        	}

	        $result = null;
        	while ( $record = mysql_fetch_array($stmt, MYSQL_ASSOC)) {
    	    	switch ($resourceSetName) {
    	    		
				case 'wp_commentmeta':
	        		
	        		$returnResult = $this->_serializewp_commentmetum($record);
       				break;
				
				case 'wp_comments':
	        		
	        		$returnResult = $this->_serializewp_comment($record);
       				break;
				
				case 'wp_links':
	        		
	        		$returnResult = $this->_serializewp_link($record);
       				break;
				
				case 'wp_options':
	        		
	        		$returnResult = $this->_serializewp_option($record);
       				break;
				
				case 'wp_postmeta':
	        		
	        		$returnResult = $this->_serializewp_postmetum($record);
       				break;
				
				case 'wp_posts':
	        		
	        		$returnResult = $this->_serializewp_post($record);
       				break;
				
				case 'wp_term_relationships':
	        		
	        		$returnResult = $this->_serializewp_term_relationship($record);
       				break;
				
				case 'wp_term_taxonomy':
	        		
	        		$returnResult = $this->_serializewp_term_taxonomy($record);
       				break;
				
				case 'wp_terms':
	        		
	        		$returnResult = $this->_serializewp_term($record);
       				break;
				
				case 'wp_usermeta':
	        		
	        		$returnResult = $this->_serializewp_usermetum($record);
       				break;
				
				case 'wp_users':
	        		
	        		$returnResult = $this->_serializewp_user($record);
       				break;
				
        		}
        	}	
        	mysql_free_result($stmt);
        	return $returnResult;        
    	}
    	
	    /**
    	 * Gets a related entity instance from an entity set identifed by a key
	     * 
    	 * @param ResourceSet      $sourceResourceSet    The entity set related to
	     *                                               the entity to be fetched.
    	 * @param object           $sourceEntityInstance The related entity instance.
     	 * @param ResourceSet      $targetResourceSet    The entity set from which
     	 *                                               entity needs to be fetched.
     	 * @param ResourceProperty $targetProperty       The metadata of the target 
     	 *                                               property.
     	 * @param KeyDescriptor    $keyDescriptor        The key to identify the entity 
     	 *                                               to be fetched.
     	 * 
     	 * @return Object/NULL Returns entity instance if found else null
     	 */
    	public function  getResourceFromRelatedResourceSet(ResourceSet $sourceResourceSet, 
        	$sourceEntityInstance, 
        	ResourceSet $targetResourceSet,
        	ResourceProperty $targetProperty,
        	KeyDescriptor $keyDescriptor
    	) {
        	$result = array();
        	$srcClass = get_class($sourceEntityInstance);
        	$navigationPropName = $targetProperty->getName();
        	$key = null;
        	foreach ($keyDescriptor->getValidatedNamedValues() as $keyName => $valueDescription) {
	            $key = $key . $keyName . '=' . $valueDescription[0] . ' and ';
    	    }
        	$key = rtrim($key, ' and ');
       		if($srcClass === 'wp_commentmetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_comment')
			{		
				
			}	
			
			else if($srcClass === 'wp_link')
			{		
				
			}	
			
			else if($srcClass === 'wp_option')
			{		
				
			}	
			
			else if($srcClass === 'wp_postmetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_post')
			{		
				
			}	
			
			else if($srcClass === 'wp_term_relationship')
			{		
				
			}	
			
			else if($srcClass === 'wp_term_taxonomy')
			{		
				
			}	
			
			else if($srcClass === 'wp_term')
			{		
				
			}	
			
			else if($srcClass === 'wp_usermetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_user')
			{		
				
			}	
			
       		return empty($result) ? null : $result[0];	
		}
		
    
	    /**
    	 * Get related resource set for a resource
     	* 
     	* @param ResourceSet      $sourceResourceSet    The source resource set
     	* @param mixed            $sourceEntityInstance The resource
     	* @param ResourceSet      $targetResourceSet    The resource set of 
     	*                                               the navigation property
     	* @param ResourceProperty $targetProperty       The navigation property to be 
     	*                                               retrieved
     	*                                               
     	* @return array(Objects)/array() Array of related resource if exists, if no 
     	*                                related resources found returns empty array
     	*/
    	public function  getRelatedResourceSet(ResourceSet $sourceResourceSet, 
        	$sourceEntityInstance, 
        	ResourceSet $targetResourceSet,
        	ResourceProperty $targetProperty,
	        $filterOption = null,
    	    $select=null, $orderby=null, $top=null, $skip=null
    	) {
	        $result = array();
    	    $srcClass = get_class($sourceEntityInstance);
	        $navigationPropName = $targetProperty->getName();
       		if($srcClass === 'wp_commentmetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_comment')
			{		
				
			}	
			
			else if($srcClass === 'wp_link')
			{		
				
			}	
			
			else if($srcClass === 'wp_option')
			{		
				
			}	
			
			else if($srcClass === 'wp_postmetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_post')
			{		
				
			}	
			
			else if($srcClass === 'wp_term_relationship')
			{		
				
			}	
			
			else if($srcClass === 'wp_term_taxonomy')
			{		
				
			}	
			
			else if($srcClass === 'wp_term')
			{		
				
			}	
			
			else if($srcClass === 'wp_usermetum')
			{		
				
			}	
			
			else if($srcClass === 'wp_user')
			{		
				
			}	
			
       		return $result;	        
    	}    
    	
	    /**
    	 * Get related resource for a resource
     	* 
     	* @param ResourceSet      $sourceResourceSet    The source resource set
     	* @param mixed            $sourceEntityInstance The source resource
     	* @param ResourceSet      $targetResourceSet    The resource set of 
     	*                                               the navigation property
     	* @param ResourceProperty $targetProperty       The navigation property to be 
     	*                                               retrieved
     	* 
     	* @return Object/null The related resource if exists else null
     	*/
    	public function getRelatedResourceReference(ResourceSet $sourceResourceSet, 
        	$sourceEntityInstance, 
        	ResourceSet $targetResourceSet,
        	ResourceProperty $targetProperty
    	) {
        	$result = null;
        	$srcClass = get_class($sourceEntityInstance);
        	$navigationPropName = $targetProperty->getName();
			if($srcClass==='wp_commentmetum')
			{
										
			}
				
			else if($srcClass==='wp_comment')
			{
										
			}
				
			else if($srcClass==='wp_link')
			{
										
			}
				
			else if($srcClass==='wp_option')
			{
										
			}
				
			else if($srcClass==='wp_postmetum')
			{
										
			}
				
			else if($srcClass==='wp_post')
			{
										
			}
				
			else if($srcClass==='wp_term_relationship')
			{
										
			}
				
			else if($srcClass==='wp_term_taxonomy')
			{
										
			}
				
			else if($srcClass==='wp_term')
			{
										
			}
				
			else if($srcClass==='wp_usermetum')
			{
										
			}
				
			else if($srcClass==='wp_user')
			{
										
			}
				
			return $result;
		}
			
		
		/**
    	 * Serialize the sql result array into wp_commentmetum objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_commentmetas($result)
    	{
        	$wp_commentmetas = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_commentmetas[] = $this->_serializewp_commentmetum($record);
        	}
        	return $wp_commentmetas;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_commentmetum object
	     * 
    	 * @param array $record each row of wp_commentmetum
	     * 
    	 * @return Object
	     */
	    private function _serializewp_commentmetum($record)
    	{
        	$wp_commentmetum = new wp_commentmetum();
        	
    		return $wp_commentmetum;
		}										
			
		/**
    	 * Serialize the sql result array into wp_comment objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_comments($result)
    	{
        	$wp_comments = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_comments[] = $this->_serializewp_comment($record);
        	}
        	return $wp_comments;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_comment object
	     * 
    	 * @param array $record each row of wp_comment
	     * 
    	 * @return Object
	     */
	    private function _serializewp_comment($record)
    	{
        	$wp_comment = new wp_comment();
        	
			$wp_comment->comment_ID = $record['comment_ID'];							
								
			$wp_comment->comment_post_ID = $record['comment_post_ID'];							
								
			$wp_comment->comment_author = $record['comment_author'];							
								
			$wp_comment->comment_author_email = $record['comment_author_email'];							
								
			$wp_comment->comment_author_url = $record['comment_author_url'];							
								
			$wp_comment->comment_author_IP = $record['comment_author_IP'];							
								
			$wp_comment->comment_date = $record['comment_date'];							
								
			$wp_comment->comment_date_gmt = $record['comment_date_gmt'];							
								
			$wp_comment->comment_content = $record['comment_content'];							
								
			$wp_comment->comment_karma = $record['comment_karma'];							
								
			$wp_comment->comment_approved = $record['comment_approved'];							
								
			$wp_comment->comment_agent = $record['comment_agent'];							
								
			$wp_comment->comment_type = $record['comment_type'];							
								
			$wp_comment->comment_parent = $record['comment_parent'];							
								
			$wp_comment->user_id = $record['user_id'];							
								
    		return $wp_comment;
		}										
			
		/**
    	 * Serialize the sql result array into wp_link objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_links($result)
    	{
        	$wp_links = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_links[] = $this->_serializewp_link($record);
        	}
        	return $wp_links;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_link object
	     * 
    	 * @param array $record each row of wp_link
	     * 
    	 * @return Object
	     */
	    private function _serializewp_link($record)
    	{
        	$wp_link = new wp_link();
        	
			$wp_link->link_id = $record['link_id'];							
								
			$wp_link->link_url = $record['link_url'];							
								
			$wp_link->link_name = $record['link_name'];							
								
			$wp_link->link_image = $record['link_image'];							
								
			$wp_link->link_target = $record['link_target'];							
								
			$wp_link->link_description = $record['link_description'];							
								
			$wp_link->link_visible = $record['link_visible'];							
								
			$wp_link->link_owner = $record['link_owner'];							
								
			$wp_link->link_rating = $record['link_rating'];							
								
			$wp_link->link_updated = $record['link_updated'];							
								
			$wp_link->link_rel = $record['link_rel'];							
								
			$wp_link->link_notes = $record['link_notes'];							
								
			$wp_link->link_rss = $record['link_rss'];							
								
    		return $wp_link;
		}										
			
		/**
    	 * Serialize the sql result array into wp_option objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_options($result)
    	{
        	$wp_options = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_options[] = $this->_serializewp_option($record);
        	}
        	return $wp_options;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_option object
	     * 
    	 * @param array $record each row of wp_option
	     * 
    	 * @return Object
	     */
	    private function _serializewp_option($record)
    	{
        	$wp_option = new wp_option();
        	
			$wp_option->option_id = $record['option_id'];							
								
			$wp_option->option_name = $record['option_name'];							
								
			$wp_option->option_value = $record['option_value'];							
								
			$wp_option->autoload = $record['autoload'];							
								
    		return $wp_option;
		}										
			
		/**
    	 * Serialize the sql result array into wp_postmetum objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_postmetas($result)
    	{
        	$wp_postmetas = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_postmetas[] = $this->_serializewp_postmetum($record);
        	}
        	return $wp_postmetas;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_postmetum object
	     * 
    	 * @param array $record each row of wp_postmetum
	     * 
    	 * @return Object
	     */
	    private function _serializewp_postmetum($record)
    	{
        	$wp_postmetum = new wp_postmetum();
        	
    		return $wp_postmetum;
		}										
			
		/**
    	 * Serialize the sql result array into wp_post objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_posts($result)
    	{
        	$wp_posts = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_posts[] = $this->_serializewp_post($record);
        	}
        	return $wp_posts;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_post object
	     * 
    	 * @param array $record each row of wp_post
	     * 
    	 * @return Object
	     */
	    private function _serializewp_post($record)
    	{
        	$wp_post = new wp_post();
        	
			$wp_post->ID = $record['ID'];							
								
			$wp_post->post_author = $record['post_author'];							
								
			$wp_post->post_date = $record['post_date'];							
								
			$wp_post->post_date_gmt = $record['post_date_gmt'];							
								
			$wp_post->post_content = $record['post_content'];							
								
			$wp_post->post_title = $record['post_title'];							
								
			$wp_post->post_excerpt = $record['post_excerpt'];							
								
			$wp_post->post_status = $record['post_status'];							
								
			$wp_post->comment_status = $record['comment_status'];							
								
			$wp_post->ping_status = $record['ping_status'];							
								
			$wp_post->post_password = $record['post_password'];							
								
			$wp_post->post_name = $record['post_name'];							
								
			$wp_post->to_ping = $record['to_ping'];							
								
			$wp_post->pinged = $record['pinged'];							
								
			$wp_post->post_modified = $record['post_modified'];							
								
			$wp_post->post_modified_gmt = $record['post_modified_gmt'];							
								
			$wp_post->post_content_filtered = $record['post_content_filtered'];							
								
			$wp_post->post_parent = $record['post_parent'];							
								
			$wp_post->guid = $record['guid'];							
								
			$wp_post->menu_order = $record['menu_order'];							
								
			$wp_post->post_type = $record['post_type'];							
								
			$wp_post->post_mime_type = $record['post_mime_type'];							
								
			$wp_post->comment_count = $record['comment_count'];							
								
    		return $wp_post;
		}										
			
		/**
    	 * Serialize the sql result array into wp_term_relationship objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_term_relationships($result)
    	{
        	$wp_term_relationships = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_term_relationships[] = $this->_serializewp_term_relationship($record);
        	}
        	return $wp_term_relationships;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_term_relationship object
	     * 
    	 * @param array $record each row of wp_term_relationship
	     * 
    	 * @return Object
	     */
	    private function _serializewp_term_relationship($record)
    	{
        	$wp_term_relationship = new wp_term_relationship();
        	
			$wp_term_relationship->object_id = $record['object_id'];							
								
			$wp_term_relationship->term_taxonomy_id = $record['term_taxonomy_id'];							
								
			$wp_term_relationship->term_order = $record['term_order'];							
								
    		return $wp_term_relationship;
		}										
			
		/**
    	 * Serialize the sql result array into wp_term_taxonomy objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_term_taxonomies($result)
    	{
        	$wp_term_taxonomies = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_term_taxonomies[] = $this->_serializewp_term_taxonomy($record);
        	}
        	return $wp_term_taxonomies;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_term_taxonomy object
	     * 
    	 * @param array $record each row of wp_term_taxonomy
	     * 
    	 * @return Object
	     */
	    private function _serializewp_term_taxonomy($record)
    	{
        	$wp_term_taxonomy = new wp_term_taxonomy();
        	
			$wp_term_taxonomy->term_taxonomy_id = $record['term_taxonomy_id'];							
								
			$wp_term_taxonomy->term_id = $record['term_id'];							
								
			$wp_term_taxonomy->taxonomy = $record['taxonomy'];							
								
			$wp_term_taxonomy->description = $record['description'];							
								
			$wp_term_taxonomy->parent = $record['parent'];							
								
			$wp_term_taxonomy->count = $record['count'];							
								
    		return $wp_term_taxonomy;
		}										
			
		/**
    	 * Serialize the sql result array into wp_term objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_terms($result)
    	{
        	$wp_terms = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_terms[] = $this->_serializewp_term($record);
        	}
        	return $wp_terms;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_term object
	     * 
    	 * @param array $record each row of wp_term
	     * 
    	 * @return Object
	     */
	    private function _serializewp_term($record)
    	{
        	$wp_term = new wp_term();
        	
			$wp_term->term_id = $record['term_id'];							
								
			$wp_term->name = $record['name'];							
								
			$wp_term->slug = $record['slug'];							
								
			$wp_term->term_group = $record['term_group'];							
								
    		return $wp_term;
		}										
			
		/**
    	 * Serialize the sql result array into wp_usermetum objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_usermetas($result)
    	{
        	$wp_usermetas = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_usermetas[] = $this->_serializewp_usermetum($record);
        	}
        	return $wp_usermetas;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_usermetum object
	     * 
    	 * @param array $record each row of wp_usermetum
	     * 
    	 * @return Object
	     */
	    private function _serializewp_usermetum($record)
    	{
        	$wp_usermetum = new wp_usermetum();
        	
    		return $wp_usermetum;
		}										
			
		/**
    	 * Serialize the sql result array into wp_user objects
		 * 	
     	 * @param array(array) $result result of the sql query
     	 * 
     	 * @return array(Object)
     	 */
    	private function _serializewp_users($result)
    	{
        	$wp_users = array();
        	while ($record = mysql_fetch_array($result, MYSQL_ASSOC)) {         
            	$wp_users[] = $this->_serializewp_user($record);
        	}
        	return $wp_users;
    	}
    	
    	/**
    	 * Serialize the sql row into wp_user object
	     * 
    	 * @param array $record each row of wp_user
	     * 
    	 * @return Object
	     */
	    private function _serializewp_user($record)
    	{
        	$wp_user = new wp_user();
        	
			$wp_user->ID = $record['ID'];							
								
			$wp_user->user_login = $record['user_login'];							
								
			$wp_user->user_pass = $record['user_pass'];							
								
			$wp_user->user_nicename = $record['user_nicename'];							
								
			$wp_user->user_email = $record['user_email'];							
								
			$wp_user->user_url = $record['user_url'];							
								
			$wp_user->user_registered = $record['user_registered'];							
								
			$wp_user->user_activation_key = $record['user_activation_key'];							
								
			$wp_user->user_status = $record['user_status'];							
								
			$wp_user->display_name = $record['display_name'];							
								
    		return $wp_user;
		}										
			
    /**
    * The destructor
    */
    public function __destruct()
    {
    if ($this->_connectionHandle) {
    mysql_close($this->_connectionHandle);
    }
    }

    public function getExpressionProvider()
    {
    if (is_null($this->_expressionProvider)) {
    $this->_expressionProvider = new gizur_comDSExpressionProvider();
    }

    return $this->_expressionProvider;
    }
    }


  
?>
	