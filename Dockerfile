# OData producer on top of MySQL and Apache
#
# VERSION               0.0.1

FROM       ubuntu:latest

# Format: MAINTAINER Name <email@addr.ess>
MAINTAINER Jonas Colmsjö <jonas@gizur.com>

RUN apt-get update
RUN apt-get install -y curl wget nano unzip

RUN echo "export HOME=/root" > /.profile


# Keep upstart from complaining
#RUN dpkg-divert --local --rename --add /sbin/initctl
#RUN ln -s /bin/true /sbin/initctl

RUN apt-get update


#
# Install PHP and phpbrew to manage PHP versions
#

RUN apt-get install -y build-essential php5 libcurl4-openssl-dev php5-enchant libenchant-dev libpng12-dev libgmp3-dev libc-client2007e-dev
RUN apt-get install -y php5 php5-dev php-pear autoconf automake curl build-essential libxslt1-dev re2c libxml2 libxml2-dev php5-cli bison libbz2-dev libreadline-dev
RUN apt-get install -y mysql-server mysql-client libmysqlclient-dev libmysqld-dev

# Fix
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h

RUN curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
RUN chmod +x phpbrew
RUN mv phpbrew /usr/bin/phpbrew

RUN apt-get install -y libmcrypt-dev

RUN /bin/bash -c "phpbrew init; . ~/.phpbrew/bashrc; phpbrew install 5.3.28 +default+mysql+pdo+intl"
RUN /bin/bash -c ". ~/.phpbrew/bashrc; phpbrew switch php-5.3.28"


#
# Install supervidord (used to handle processes)
#

RUN apt-get install -y supervisor
ADD ./supervisord.conf /etc/supervisor/conf.d/supervisord.conf


#
# Install Apache
#

#RUN apt-get install -y apache2 php5 php5-curl php5-mysql php5-mcrypt


# Install apache
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y apache2 php5-curl php5-mysql php5-mcrypt
ADD ./conf/etc /etc

# Use the same php.ini for CLI
ADD ./conf/etc/php5/apache2/php.ini /etc/php5/cli/php.ini

RUN a2enmod rewrite

# Install phpMyAdmin, tar.gz file are automatically unpacked (strange)
ADD ./src-phpmyadmin/phpMyAdmin-4.0.8-all-languages.tar.gz /var/www/html/
#RUN cd /var/www/html; tar -xzf phpMyAdmin-4.0.8-all-languages.tar.gz
RUN rm /var/www/html/index.html

ADD ./src-phpmyadmin/config.inc.php /var/www/html/phpMyAdmin-4.0.8-all-languages/config.inc.php

# For debugging purposes
RUN echo "<?php\nphpinfo();" > /var/www/html/info.php

#
# Install Wordpress
#

#ADD ./src-wordpress /var/www/html


#
# Install MySQL
#

# Bundle everything
ADD ./src-mysql /src-mysql

# Load wordpress SQL dump
ADD ./sql-script/latest.sql /sql-script/latest.sql

# Install MySQL server
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server && apt-get clean && rm -rf /var/lib/apt/lists/*

# Fix configuration
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Setup admin user
RUN /src-mysql/mysql-setup.sh


#
# Install OData
#

ADD ./MySQLConnectorV1.0.zip /
ADD ./ODataProducerLibV1.1.zip /

RUN mkdir /MySQLConnector; unzip MySQLConnectorV1.0.zip -d /MySQLConnector
RUN unzip /ODataProducerLibV1.1.zip


RUN mv /OData\ Producer\ for\ PHP/ /var/www/html/ODataProducer
ADD ./var-www-htaccess /var/www/html/.htaccess

# Add the example service
ADD ./example/gizur_com /var/www/html/ODataProducer/services/
RUN mkdir -p /var/www/html/ODataProducer/services/gizur_com
RUN mv /var/www/html/ODataProducer/services/gizur_com* /var/www/html/ODataProducer/services/gizur_com || true

# Add config file
ADD ./example/gizur_com/service.config.xml /var/www/html/ODataProducer/services/	

# Install composer
RUN curl -sS https://getcomposer.org/installer | php

# Install Doctrine
RUN echo '{"require": {"doctrine/dbal": "2.3.4"}}' > /MySQLConnector/composer.json
RUN cd /MySQLConnector; php /composer.phar install
RUN mkdir /MySQLConnector/Doctrine
RUN ln -s /MySQLConnector/vendor/doctrine/dbal/lib/Doctrine/DBAL /MySQLConnector/Doctrine/DBAL
RUN ln -s /MySQLConnector/vendor/doctrine/common/lib/Doctrine/Common /MySQLConnector/Doctrine/Common

# Some file needs patching

ADD ./src-fixed-odata/MetadataAssociationTypeSet.php /var/www/html/ODataProducer/library/ODataProducer/Writers/Metadata/

ADD ./src-fixed-odata/ServiceBaseMetadata.php /var/www/html/ODataProducer/library/ODataProducer/Providers/Metadata/

ADD ./src-fixed-odata/IDataServiceStreamProvider2.php /var/www/html/ODataProducer/library/ODataProducer/Providers/Stream/


#
# Start things
#

RUN mkdir /volume
VOLUME /volume

EXPOSE 80 443
#CMD ["supervisord","-n"]

ADD ./start.sh /
CMD ["/start.sh"]


